# deep-learning-challenge
Creator: Melissa Acevedo

### Overview

This repository contains the analysis of a deep learning model developed for Alphabet Soup Charity. The purpose of this analysis is to create a binary classifier that can predict the success of funding applications submitted to Alphabet Soup. The model will determine whether an organization's funding will be used effectively based on various features provided in the dataset.

### Files

- `AlphabetSoupCharity.ipynb`: Jupyter Notebook containing the preprocessing, model development, and evaluation for the initial neural network model.
- `AlphabetSoupCharity_Optimization.ipynb`: Jupyter Notebook containing optimization techniques applied to the neural network model.
- `AlphabetSoupCharity.h5`: HDF5 file containing the trained neural network model.
- `AlphabetSoupCharity_Optimization.h5`: HDF5 file containing the optimized neural network model.
- `charity_data.csv`: Dataset containing information about organizations that received funding from Alphabet Soup.

### Background

Alphabet Soup wants to select applicants with the best chance of success for funding. The provided dataset contains metadata about past funding recipients, including application type, affiliation, organization classification, and funding outcomes. The goal is to develop a model that can predict whether future applicants will be successful based on these features.

### Before You Begin

Follow these steps to set up the project:

1. Create a new repository named "deep-learning-challenge" for this project.
2. Clone the repository to your local machine.
3. Create a directory named "Deep Learning Challenge" inside the repository.
4. Download the necessary files provided for the challenge.
5. Upload the Jupyter Notebook files to Google Colab for analysis.
6. Start working on the preprocessing and model development tasks.

### Step 1: Preprocess the Data

- Identify the target variable(s) and feature(s) for the model.
- Drop the "EIN" and "NAME" columns.
- Determine the number of unique values for each column.
- Bin rare categorical variables together into a new value, "Other."
- Encode categorical variables using pd.get_dummies().
- Split the preprocessed data into features and target arrays.
- Scale the features datasets using StandardScaler.
- Split the data into training and testing datasets.

### Step 2: Compile, Train, and Evaluate the Model

- Design a neural network model with appropriate input features and layers.
- Compile and train the model using TensorFlow and Keras.
- Evaluate the model's performance using test data.
- Save the trained model to an HDF5 file named "AlphabetSoupCharity.h5".

### Step 3: Optimize the Model

- Implement optimization methods to achieve a target predictive accuracy higher than 75%.
- Adjust input data, increase neurons or hidden layers, use different activation functions, or modify epochs.
- Save the optimized model to an HDF5 file named "AlphabetSoupCharity_Optimization.h5".

### Step 4: Write a Report on the Neural Network Model

#### Overview of the Analysis

The analysis aims to develop a deep learning model for Alphabet Soup Charity to predict the success of funding applications. By preprocessing the dataset and optimizing the neural network model, the goal is to achieve high predictive accuracy.

#### Results

**Data Preprocessing**
- Target variable(s): IS_SUCCESSFUL
- Feature variable(s): APPLICATION_TYPE, AFFILIATION, CLASSIFICATION, USE_CASE, ORGANIZATION, STATUS, INCOME_AMT, SPECIAL_CONSIDERATIONS, ASK_AMT
- Removed variables: EIN, NAME

**Compiling, Training, and Evaluating the Model**
- Selected neurons, layers, and activation functions based on experimentation.
- Achieved an accuracy of approximately 72.5% with the initial model.
- Optimization attempts included adjusting input data and increasing model complexity.
- Final optimized model achieved similar accuracy to the initial model.

#### Summary

The deep learning model developed for Alphabet Soup Charity shows promise in predicting funding success. However, further optimization may be necessary to achieve the desired accuracy threshold. A different model, such as a random forest classifier, could also be explored to address this classification problem, considering its ability to handle complex datasets and nonlinear relationships.

